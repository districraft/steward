import org.districraft.steward.StewardMain;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class StewardMainClassTest {
    @Mock
    GameStartedServerEvent gameStartedServerEvent;

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test(expected = NotImplementedException.class)
    public void when_ServerStartEvent_should_throw_NotImplementedException() {
        StewardMain stewardMain = new StewardMain();
        stewardMain.onServerStart(gameStartedServerEvent);
    }
}
