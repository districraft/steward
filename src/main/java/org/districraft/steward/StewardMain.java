package org.districraft.steward;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Plugin(id = "steward", name = "Steward", version = "0.0.1-API7", description = "A very powerful ticketing system")
public class StewardMain {

    @Inject
    private Logger log;

    @Listener
    public void onServerStart(GameStartedServerEvent e) throws NotImplementedException {
        throw new NotImplementedException();
    }
}
